package com.example.githeko;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class WordAdapter extends RecyclerView.Adapter<WordAdapter.WordAdapterViewHolder> {




        Context context;
        List<EnglishKiswahili> englishKiswahiliList;
        public WordAdapter(Context c, List<EnglishKiswahili> englishKiswahilis){
            context=c;
            englishKiswahiliList=englishKiswahilis;
        }
        @NonNull
        @Override
        public WordAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view= LayoutInflater.from(context).inflate(R.layout.single_word_adapter,viewGroup,false);
            return  new WordAdapterViewHolder(view);

        }

        @Override
        public void onBindViewHolder(@NonNull WordAdapterViewHolder holder, int i) {

            EnglishKiswahili englishKiswahili=englishKiswahiliList.get(i);

            holder.textData.setText(englishKiswahili.kiswahili);

        }



        @Override
        public int getItemCount() {
            return englishKiswahiliList.size();
        }

        public class WordAdapterViewHolder extends RecyclerView.ViewHolder{

            TextView textData;
            public WordAdapterViewHolder(@NonNull View itemView) {
                super(itemView);

                textData=itemView.findViewById(R.id.single_en);
            }
        }
    }


