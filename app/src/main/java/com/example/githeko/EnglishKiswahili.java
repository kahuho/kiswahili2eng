package com.example.githeko;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;

public class EnglishKiswahili extends Model {

    @Column(name = "kiswahili")
    public String kiswahili;


    @Column(name = "english")
    public String english;


}
