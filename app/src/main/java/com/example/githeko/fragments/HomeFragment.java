package com.example.githeko.fragments;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.example.githeko.EnglishKiswahili;
import com.example.githeko.MainActivity;
import com.example.githeko.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HomeFragment extends Fragment {
    Button btn,convert;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn = view.findViewById(R.id.home_btn);
        convert = view.findViewById(R.id.convert_home_btn);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction=((MainActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame,new ConvertFragment(),"convert").commit();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    String mCSVfile = "database.csv";
                    AssetManager manager = getActivity().getAssets();
                    InputStream inStream = null;
                    try {
                        inStream = manager.open(mCSVfile);
                    } catch (IOException e) {
                        e.printStackTrace();

                        Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG).show();

                    }

                    BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));


                    String line = "";
                    ActiveAndroid.beginTransaction();

                    try {
                        while ((line = buffer.readLine()) != null) {
                            String[] colums = line.split(",");
                            if (colums.length != 2) {
                                Log.d("CSVParser", "Skipping Bad CSV Row");
                                continue;
                            }
                            ContentValues cv = new ContentValues(2);

                            EnglishKiswahili language=new EnglishKiswahili();
                            language.kiswahili=colums[0].trim();
                            language.english=colums[1].trim();
                            language.save();


                            Log.e("hehe",language.kiswahili);

                        }
                        ActiveAndroid.setTransactionSuccessful();

                    } catch (IOException e) {
                        e.printStackTrace();

                        Toast.makeText(getActivity(),"Error",Toast.LENGTH_LONG).show();

                    }finally {
                        ActiveAndroid.endTransaction();

                    }

                    Toast.makeText(getActivity(),"Saved", Toast.LENGTH_LONG).show();
                }



        });
    }
}
