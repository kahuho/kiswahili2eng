package com.example.githeko.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.githeko.MainActivity;
import com.example.githeko.R;

public class ConvertFragment extends Fragment {
    EditText textName;
    Button  submitBtn;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.convert_fragment, container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        submitBtn=view.findViewById(R.id.submit_btn);
        textName=view.findViewById(R.id.text_name);


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(textName.getText().toString()==""){

                    Toast.makeText(getActivity(),"The entered value is empty",Toast.LENGTH_LONG).show();
                    return;
                }
                ResultsFragment resultsFragment=new ResultsFragment();
                Bundle bundle=new Bundle();
                bundle.putString("entered_text",textName.getText().toString());

                resultsFragment.setArguments(bundle);

                FragmentTransaction  transaction=((MainActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame,resultsFragment,"results").commit();


            }
        });
    }
}
