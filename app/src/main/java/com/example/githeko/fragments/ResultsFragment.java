package com.example.githeko.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.Select;
import com.example.githeko.EnglishKiswahili;
import com.example.githeko.R;
import com.example.githeko.WordAdapter;

import java.util.List;

public class ResultsFragment extends Fragment {
        WordAdapter  wordAdapter;
        RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.results_fragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        recyclerView=view.findViewById(R.id.recycler_view);

        String text=getArguments().getString("entered_text");
        List<EnglishKiswahili> list=new Select().from(EnglishKiswahili.class).where("english=?",text).execute();

        wordAdapter=new WordAdapter(getActivity(),list);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(wordAdapter);

    }
}
