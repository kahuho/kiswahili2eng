package com.example.githeko;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;

public class Githeko extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ActiveAndroid.initialize(this);
    }
}
